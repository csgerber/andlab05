package com.deitel.addressbook;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefsMgr {

    private static SharedPreferences sSharedPreferences;



    public static void setBoolean(Context context, String key, boolean bVal) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, bVal);
        editor.commit();

    }

    public static boolean getBoolean(Context context, String key, boolean bDefault) {
        sSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return sSharedPreferences.getBoolean(key, bDefault);

    }





}